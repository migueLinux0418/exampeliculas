import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    loadChildren: ()=> import('./principal/principal.module').then(m=> m.PrincipalModule)
  },
  {
    path: 'Contacto',
    loadChildren: ()=> import('./contact-form-module/contact-form-module.module').then(m => m.ContactFormModuleModule)
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

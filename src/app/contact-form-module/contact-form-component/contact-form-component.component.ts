import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from "@angular/forms";
import Swal from 'sweetalert2';
import { Router } from '@angular/router';
import {} from '@angular/platform-browser' 


@Component({
  selector: 'app-contact-form-component',
  templateUrl: './contact-form-component.component.html',
  styleUrls: ['./contact-form-component.component.css']
})

export class ContactFormComponentComponent implements OnInit {
  form: FormGroup;
  hide: boolean = true;

  constructor(private _fb: FormBuilder,
              private _ruta: Router){
      this.form = this._fb.group({
        nombre: [null, Validators.required],
        correo: [null, Validators.required, Validators.email],
        telefono: [null, ],
        mensaje: [null, Validators.maxLength(40)]        
      })
    }

  ngOnInit(): void {
  }

  onContact(): void {
    /**Solo preguntamos si el formulario es valido */
    if (this.form.invalid) {
      this.form.markAllAsTouched();
      return;
    }
    this._ruta.navigateByUrl('');
    Swal.fire({ icon: 'success', title: "Gracias por contactarnos" });


  }

  get nombre(): FormControl {
    return <FormControl>this.form.get('nombre');
  }

  get correo(): FormControl {
    return <FormControl>this.form.get('correo');
  }
  
  get mensaje(): FormControl {
    return <FormControl>this.form.get('mensaje');
  }

}

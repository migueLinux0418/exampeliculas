import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ContactFormModuleRoutingModule } from './contact-form-module-routing.module';
import { ContactFormComponentComponent } from './contact-form-component/contact-form-component.component';
import { ShareModule } from'../share/share.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';


@NgModule({
  declarations: [
    ContactFormComponentComponent
  ],
  imports: [
    CommonModule,
    ContactFormModuleRoutingModule,
    ShareModule
  ]
})
export class ContactFormModuleModule { }

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';


import { PeliculasRoutingModule } from './peliculas-routing.module';
import { PeliculasComponent } from './peliculas/peliculas.component';
import { ShareModule } from'../share/share.module';


@NgModule({
  declarations: [
    PeliculasComponent
  ],
  imports: [
    CommonModule,
    PeliculasRoutingModule,
    ShareModule
  ]
})
export class PeliculasModule { }

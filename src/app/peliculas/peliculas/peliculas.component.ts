import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource, MatTableModule } from '@angular/material/table';
import { Ipeliculas } from '../interfaces/ipeliculas';
import { SPeliculasService } from '../services/s-peliculas.service';
import { MatPaginator } from '@angular/material/paginator';

@Component({
  selector: 'app-peliculas',
  templateUrl:'./peliculas.component.html',
  styleUrls: ['./peliculas.component.css']
})

export class PeliculasComponent implements  AfterViewInit, OnInit  {
displayedColumns: string[] = ['numero', 'name', 'year'];
dataSource = new MatTableDataSource<Ipeliculas>();
  
  //@ts-ignore
  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(private _http: SPeliculasService){
  }
  ngOnInit(): void {
    this.getPelis();
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
  }

  getPelis(): void{
    this._http.getPelis().subscribe(
      data=>{
        this.dataSource = new MatTableDataSource<Ipeliculas>(data);
        this.dataSource.paginator = this.paginator;
        console.log(data);
      },
      error=>{
        console.log(error);
      }
    );
  }

 
  }

 


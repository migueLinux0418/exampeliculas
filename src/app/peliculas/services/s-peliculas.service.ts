import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { map, Observable } from "rxjs";
import { environment } from '../../../environments/environment';
import { Ipeliculas } from '../interfaces/ipeliculas';

@Injectable({
  providedIn: 'root'
})
export class SPeliculasService {
  urlService: string = environment.url;
  constructor(private _http: HttpClient) { }

  getPelis(): Observable<Ipeliculas[]>{
    return this._http.get<Ipeliculas[]>(this.urlService);
  }

}

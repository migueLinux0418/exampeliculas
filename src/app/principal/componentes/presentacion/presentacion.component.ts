import { Component, OnInit } from '@angular/core';

import { ValMensajeService } from '../../../principal/services/val-mensaje.service'

@Component({
  selector: 'app-presentacion',
  templateUrl: './presentacion.component.html',
  styleUrls: ['./presentacion.component.css']
})

export class PresentacionComponent implements OnInit {

  constructor(private _sMsj: ValMensajeService){}
  ngOnInit(): void {
    this._sMsj.guardarMensaje();
  }
}

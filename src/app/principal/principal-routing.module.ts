import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PresentacionComponent} from './componentes/presentacion/presentacion.component'

const routes: Routes = [
  {
    path: '',
    component: PresentacionComponent
  },
  {path: 'Contacto',
  loadChildren: ()=> import('../contact-form-module/contact-form-module.module').then(m=>m.ContactFormModuleModule)
},
{
  path: 'ListaPeliculas',
  loadChildren: ()=> import('../peliculas/peliculas.module').then(m=> m.PeliculasModule)
}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PrincipalRoutingModule { }

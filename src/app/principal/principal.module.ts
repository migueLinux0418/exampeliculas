import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PrincipalRoutingModule } from './principal-routing.module';
import { PresentacionComponent } from './componentes/presentacion/presentacion.component';
import { ShareModule } from '../share/share.module';


@NgModule({
  declarations: [
    PresentacionComponent,
  ],
  imports: [
    CommonModule,
    PrincipalRoutingModule
  ]
})
export class PrincipalModule { }

import { Injectable } from '@angular/core';
import Swal from 'sweetalert2'

@Injectable({
  providedIn: 'root'
})
export class ValMensajeService {

  constructor() { }

  

  guardarMensaje(): void{
    const siMensaje: string = localStorage.getItem('mensaje') || 'no';
    if (siMensaje !== 'si') {
      Swal.fire({ icon: 'success', title: "Vienvenido" });
      localStorage.setItem('mensaje','si');
    }
    
  }

}
